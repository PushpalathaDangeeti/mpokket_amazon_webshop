*** Settings ***
Resource          ../Global/Super.robot

*** Keywords ***
Validate Cart is in Pridictable State
    Lauch Browser and Navigate to URL    ${URL}    ${BROWSER_NAME}
    ${cart_count}    SeleniumLibrary.Get Text    ${button.amazon.cart}
    ${cart_count_status}    Run Keyword And Return Status    Should Be Equal    ${cart_count}    0
    Run Keyword If    ${cart_count_status}==False    Fail and Take Screenshot    Cart is not empty

Search For Product
    [Arguments]    ${CATEGORY}    ${PRODUCT_NAME}
    SeleniumLibrary.Select From List By Label    ${dropdown.amazon.select_category}    ${CATEGORY}
    SeleniumLibrary.Input Text    ${textbox.amazon.search}    ${PRODUCT_NAME}
    SeleniumLibrary.Click Element    ${button.amazon.search}

Validate Product List
    [Arguments]    @{PRODUCT_LIST}
    ${products_count}    SeleniumLibrary.Get Element Count    ${label.amazon.products_list}
    FOR    ${key}    IN RANGE    1    ${products_count}
        ${product_name}    SeleniumLibrary.Get Text    (${label.amazon.products_list})[${key}]
        Should Contain Any    ${product_name}    @{PRODUCT_LIST}
    END

Add Product to Cart
    [Arguments]    ${quantity}
    ${product_full_name}    SeleniumLibrary.Get text    (${label.amazon.products_list})[1]
    Set Global Variable    ${product_full_name}
    SeleniumLibrary.Click Element    ${label.amazon.products_list}/span[text()='${product_full_name}']
    ${windows}    SeleniumLibrary.Get Window Handles
    SeleniumLibrary.Switch Window    ${SECOND_WINDOW}
    SeleniumLibrary.Wait Until Element Is Visible    ${dropdown.amazon.cart.quantity}    ${WAIT_TIME}    Quantity dropdown is not visible after waiting ${WAIT_TIME} seconds
    SeleniumLibrary.Select From List By Label    ${dropdown.amazon.cart.quantity}    ${quantity}
    SeleniumLibrary.Wait Until Element Is Visible    ${button.amazon.product.add_to_cart}    ${WAIT_TIME}    "Add to Cart" button is not visible after waiting ${WAIT_TIME} seconds
    SeleniumLibrary.Click Element    ${button.amazon.product.add_to_cart}

Validate Product is Added in Cart
    SeleniumLibrary.Click Element    ${button.amazon.cart}
    SeleniumLibrary.Wait Until Element Is Visible    //span[@class='a-list-item']/a/span[contains(.,'${product_full_name}')]    ${WAIT_TIME}    Added product is not visible in cart after waiting ${WAIT_TIME} seconds
    ${cart_count}    SeleniumLibrary.Get Text    ${button.amazon.cart}
    ${cart_status}    Run Keyword And Return Status    Should Not Be Equal    ${cart_count}    0
    Run Keyword If    ${cart_status}==False    Fail and Take Screenshot    The products count ${cart_count} in cart is equal to 0
    ${single_product_price}    Get Total Price of Products Added in Cart
    Set Global Variable    ${single_product_price}

Get Total Price of Products Added in Cart
    SeleniumLibrary.Click Element    ${button.amazon.cart}
    Comment    Sleep    ${WAIT_TIME}
    SeleniumLibrary.Wait Until Element Is Visible    ${label.amazon.cart.total_price}    ${WAIT_TIME}    Sub total price is not visible after waiting ${WAIT_TIME} seconds
    ${total_price}    SeleniumLibrary.Get Text    ${label.amazon.cart.total_price}
    [Return]    ${total_price}

Increase the Number of Items in Cart
    [Arguments]    ${product_quantity}
    Search For Product    ${CATEGORY}    ${PRODUCT_NAME}
    Add Product to Cart    ${product_quantity}

Validate Single Product to Multiple Product Total Price in Cart
    ${Increased_products_total_price}    Get Total Price of Products Added in Cart
    ${total_price}    Set Variable    ${Increased_products_total_price}
    ${price_validation}    Run Keyword And Return Status    Should Not Be Equal    ${total_price}    ${single_product_price}
    Run Keyword If    ${price_validation}==False    Fail and Take Screenshot    Multi products price:${Increased_products_total_price} is not equal to the expected price ${expected_total_price}

Remove Product from Cart
    SeleniumLibrary.Click Element    ${button.amazon.cart}
    SeleniumLibrary.Wait Until Element Is Visible    ${button.amazon.cart.delete}    ${WAIT_TIME}    Delete button is not displayed after waiting ${WAIT_TIME} seconds
    SeleniumLibrary.Click Element    ${button.amazon.cart.delete}
    SeleniumLibrary.Wait Until Element Is Visible    ${label.amazon.empty_cart_message}    ${WAIT_TIME}    Empty cart message is not displayed after waiting ${WAIT_TIME} seconds

Validate Empty Cart Message
    [Arguments]    ${expected_empty_cart_message}
    SeleniumLibrary.Click Element    ${button.amazon.cart}
    SeleniumLibrary.Wait Until Element Is Visible    ${label.amazon.empty_cart_message}    ${WAIT_TIME}    Empty cart message is not displayed after waiting ${WAIT_TIME} seconds
    ${actual_empty_cart_message}    SeleniumLibrary.Get Text    ${label.amazon.empty_cart_message}
    ${message_status}    Run Keyword And Return Status    Should Be Equal    ${actual_empty_cart_message}    ${actual_empty_cart_message}
    Run Keyword If    ${message_status}==False    Fail and Take Screenshot    ${actual_empty_cart_message} is not equal to ${actual_empty_cart_message}

Fail and Take Screenshot
    [Arguments]    ${message}
    SeleniumLibrary.Capture Page Screenshot
    Fail    ${message}
