*** Settings ***
Resource          ../Global/Super.robot

*** Keywords ***
Lauch Browser and Navigate to URL
    [Arguments]    ${url}    ${browser_name}
    SeleniumLibrary.Open Browser    ${url}    ${browser_name}
    SeleniumLibrary.Maximize Browser Window
