*** Variables ***
${BROWSER_NAME}    chrome
${URL}            https://www.amazon.in
${PRODUCT_NAME}    Masks
${CATEGORY}       All Categories
${WAIT_TIME}      10sec
${PRODUCT_QUANTITY}    10
@{PRODUCT_LIST}    mask    Mask    MASK    Face
${EMPTY_CART_MESSAGE}    Your Amazon Basket is empty
${SINGLE_QUANTITY}    1
${MAIN_WINDOW}    MAIN
${SECOND_WINDOW}    NEW
