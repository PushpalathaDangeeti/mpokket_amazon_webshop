*** Settings ***
Library           SeleniumLibrary
Resource          ../Keywords/Common.robot
Resource          Global_variables.robot
Resource          ../Keywords/Amazon.robot
Resource          ../ObjectRepository/buttons.robot
Resource          ../ObjectRepository/dropdowns.robot
Resource          ../ObjectRepository/textboxes.robot
Resource          ../ObjectRepository/labels.robot
