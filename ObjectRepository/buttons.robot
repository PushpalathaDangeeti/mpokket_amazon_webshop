*** Variables ***
${button.amazon.cart}    nav-cart-count
${button.amazon.cart.delete}    //input[@value='Delete']
${button.amazon.search}    nav-search-submit-button
${button.amazon.product.add_to_cart}    add-to-cart-button
${button.amazon.product.cart.update}    //a[contains(.,'Update')]
