*** Settings ***
Documentation     1. Search for a product. Verify the list.
...               2. Add a product to the cart. Verify that the added product is in the cart.
...               3. Increase the number of items in the cart to 10 Verify that the total price changed.
...               4. Remove a product from the cart. Verify the change.
Suite Setup       Validate Cart is in Pridictable State
Resource          ../Global/Super.robot

*** Test Cases ***
Search for Product and Verify the Product List
    [Documentation]    Search for a product. Verify the list.
    Comment    Search for the given product
    Search For Product    ${CATEGORY}    ${PRODUCT_NAME}
    Comment    Validate the product list
    Validate Product List    @{PRODUCT_LIST}

Add Product to Cart and Validate Added Product in Cart
    [Documentation]    Add a product to the cart. Verify that the added product is in the cart.
    Comment    Add first product to cart
    Add Product to Cart    ${SINGLE_QUANTITY}
    Comment    Validate the added product in cart
    Validate Product is Added in Cart

Validate Total Price Once Extra Products Added to the Cart
    [Documentation]    Increase the number of items in the cart to 10 Verify that the total price changed.
    Comment    Increase the product quantity
    Increase the Number of Items in Cart    ${PRODUCT_QUANTITY}
    Comment    Validate the single product price and multiple products total price
    Validate Single Product to Multiple Product Total Price in Cart

Validate Change After Product Removed from the Cart
    [Documentation]    Remove a product from the cart. Verify the change.
    Comment    Remove the products from the cart
    Remove Product from Cart
    Comment    Validate all products removed from cart and message
    Validate Empty Cart Message    ${EMPTY_CART_MESSAGE}
